const path = require('path');
//const CopyPlugin = require('copy-webpack-plugin');

module.exports = [
  {
    entry: './src/js/MemoJS.js',
    output: {
      filename: 'MemoJS.js',
      path: path.join(__dirname, 'build', 'js'),
      libraryTarget: 'umd',
      library: 'main'
    },
    bail: true,
    devtool: 'source-map',
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env']
              }
            },
          ]
        },
        {
          test: /\.css$/i,
          use: ['style-loader', 'css-loader'],
        },
      ]
    },
    /* plugins: [
      new CopyPlugin({
        patterns: [
          //{ from: 'src/dist/index.html', to: '../index.html' },
          //{ from: 'src/css', to: '../css' },
          { from: 'package.json', to: '../' },
          { from: 'README.md', to: '../' },
        ],
      }),
    ], */
  },
]; // module exports
