"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "MemoJS", {
  enumerable: true,
  get: function get() {
    return _MemoJSizer["default"];
  }
});
var _MemoJSizer = _interopRequireDefault(require("./MemoJSizer"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
//# sourceMappingURL=MemoJS.js.map