"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _markdownIt = _interopRequireDefault(require("markdown-it"));
var _markdownItAttrs = _interopRequireDefault(require("markdown-it-attrs"));
var _markdownItHtml5EmbedFix = _interopRequireDefault(require("markdown-it-html5-embed-fix"));
var _core = _interopRequireDefault(require("highlight.js/lib/core"));
var _javascript = _interopRequireDefault(require("highlight.js/lib/languages/javascript"));
var _xml = _interopRequireDefault(require("highlight.js/lib/languages/xml"));
var _css2 = _interopRequireDefault(require("highlight.js/lib/languages/css"));
var _utils = require("./utils");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); } //import markdown-it
//import markdown-it.attrs
//highlight.js configuration
_core["default"].registerLanguage('javascript', _javascript["default"]);
_core["default"].registerLanguage('html', _xml["default"]);
_core["default"].registerLanguage('css', _css2["default"]);
var MemoJSizer = /*#__PURE__*/function () {
  function MemoJSizer(MDEntryPath) {
    _classCallCheck(this, MemoJSizer);
    this.MDEntryPath = MDEntryPath; //"md/index.md"

    //register the markdown converter
    this.converter = undefined;
    this.renderString = "render";
    this.renderStringRegEx = /render-[a-z]/;
    this.allRenderStringRegEx = /render-[a-z]/g;

    //list of all what we want to render
    this.toRender = {};
    this.setup();
  }

  //setup callback after page loading
  _createClass(MemoJSizer, [{
    key: "setup",
    value: function setup() {
      var _this = this;
      //adds the article tag with the needed id and class name
      document.body.innerHTML = "<article id='markdown-body' class='markdown-body'></article>";

      //launch the observer
      this.onAppend(document.getElementById("markdown-body"), function (added) {
        //walk through added nodes
        for (var i = 0; i < added.length; i++) {
          var elem = added[i];
          //console.log("added element = ", elem, elem.lang);

          //act only on <pre> element for detecting html, css and js code
          if (elem.tagName === "PRE") {
            //grab the script from the element
            var codeText = elem.innerText;
            var codeTag = elem.children[0];
            var scriptHTML = codeTag.innerHTML;
            var renderMatch = scriptHTML.match(_this.allRenderStringRegEx);

            //console.log("match", renderMatch, scriptHTML);

            if (renderMatch) {
              for (var _i = 0; _i < renderMatch.length; _i++) {
                var renderRef = renderMatch[_i];
                //console.log("match str", renderRef);

                //erase all the render-n instances
                var tempScriptHTML = scriptHTML.replace(_this.allRenderStringRegEx, "");
                codeTag.innerHTML = tempScriptHTML;
                codeText = codeTag.innerText;

                //we must erase all links in the HTML file!
                if (elem.lang === "html") {
                  //make a fake html document to manipulate the elements freely
                  var fakeDoc = document.createElement("html");
                  //affects the current html doc text (clean all highlight.js tags!)
                  fakeDoc.innerHTML = codeText;

                  //grab the elements we want to clear, and remove them
                  var links = fakeDoc.getElementsByTagName("link");
                  for (var _i2 = 0; _i2 < links.length; _i2++) {
                    var link = links[_i2];
                    link.parentElement.removeChild(link);
                  }
                  var scripts = fakeDoc.getElementsByTagName("script");
                  for (var _i3 = 0; _i3 < scripts.length; _i3++) {
                    var script = scripts[_i3];
                    script.parentElement.removeChild(script);
                  }
                  //replace the html source text with the new one
                  codeText = fakeDoc.innerHTML;
                  //console.log("html codeText", codeText);
                }

                //if don't have already the object to keep track of all codes
                if (!Object.getOwnPropertyDescriptor(_this.toRender, renderRef)) {
                  _this.toRender[renderRef] = {}; //create {render-n : }
                  _this.toRender[renderRef][elem.lang] = codeText; //fill {render-n : {lang: }}

                  _this.toRender[renderRef]["elems"] = []; //fill {render-n : {lang: , elems: }}
                  _this.toRender[renderRef]["elems"].push(elem);
                } else {
                  _this.toRender[renderRef][elem.lang] = codeText; //fill {render-n : {lang: }}
                  _this.toRender[renderRef]["elems"].push(elem); //add to {render-n : {lang: , elems: }}
                }
              }
            }
          }

          //find the last element of the page, always the same "retour à l'index"
          if (elem.tagName === "P") {
            if (elem.innerText === "Fin.") {
              //console.log("the last item!");
              //console.log("toRender", this.toRender);

              //now generates the iFrames with the code we defined
              _this.addIFrames();
            }
          }
        }
      });

      //check if we're already in one of the lesson page or not
      var MDPath = (0, _utils.getUrlParameter)(window.location.href, "mdpath");

      //if not, we'll get the main index for all lessons
      if (!MDPath) {
        MDPath = this.MDEntryPath;
      }
      //console.info("MDPath", MDPath);

      //markdown-it
      this.converter = new _markdownIt["default"]("default", {
        "highlight": function highlight(str, lang) {
          //console.log("lang", lang);

          if (lang && _core["default"].getLanguage(lang)) {
            try {
              var preTag = '<pre class="hljs" lang=' + lang + '><code class>' + _core["default"].highlight(lang, str, true).value + '</code></pre>';
              return preTag;
            } catch (__) {}
          }
        }
      });
      this.converter.use(_markdownItAttrs["default"]);
      this.converter.use(_markdownItHtml5EmbedFix["default"], {
        html5embed: {
          useImageSyntax: true,
          // Enables video/audio embed with ![]() syntax (default)
          useLinkSyntax: true,
          // Enables video/audio embed with []() syntax
          attributes: {
            video: 'class="video-player" controls'
          }
        }
      });

      //Load the markdown file and convert it to HTML with the converter
      (0, _utils.load)(MDPath, function (text) {
        //var html = converter.makeHtml(md);
        var html = _this.converter.render(text);
        document.getElementById("markdown-body").innerHTML = html;

        //automatic generation of md files links for inner navigation
        var links = document.getElementsByTagName("a");
        for (var i = 0; i < links.length; i++) {
          var link = links[i];
          link.addEventListener("click", function (event) {
            var url = new URL(this.href);
            var newMDpath = (0, _utils.getUrlParameter)(url, "mdpath");
            if (newMDpath) {
              var newURL = window.location.href + "?mdpath=" + newMDpath;
              //console.log("newURL", newURL);
              window.location.replace(newURL);
            }
          });
        }
      });
    }

    /**
     * adds rendering iFrames from code sources
     */
  }, {
    key: "addIFrames",
    value: function addIFrames() {
      for (var renderObj in this.toRender) {
        var render = this.toRender[renderObj];

        //console.log("current render", render);

        //build the iFrame
        var iframe = document.createElement("iframe");
        iframe.src = 'about:blank';
        iframe.style.border = "solid gray 1px";
        iframe.style.width = "100%";
        iframe.style.height = "300px";
        iframe.style.overflow = "hidden";
        iframe.style.MozOverflow = "hidden";
        iframe.style.resize = "vertical";

        //what element is used to add an new one before
        var lastElem = void 0;
        var _html = void 0,
          js = void 0,
          _css = void 0;

        //iterate through all keys (html, css, ...)
        for (var key in render) {
          //finds the last HTML element to use as a ref
          if (key === "elems") {
            lastElem = render[key][render[key].length - 1];
            //console.log(lastElem);
            //add it to the document to generate the contentWindow
            lastElem.parentNode.insertBefore(iframe, lastElem.nextSibling);
          }
          if (key === "html") {
            _html = render[key];
          }
          if (key === "javascript") {
            js = render[key];
          }
          if (key === "css") {
            _css = render[key];
          }
        }
        setTimeout(this.applyToIFrame, 100, iframe, _html, _css, js);
      }
    }
  }, {
    key: "applyToIFrame",
    value: function applyToIFrame(iframe, html, css, js) {
      //change the content of the iFrame

      //html first, as we will change it's structure with JS afterward
      if (html) {
        //console.log("setHTML", html);
        iframe.contentDocument.write(html);
      }

      //css next
      if (css) {
        //console.log("setCSS", css);
        var styleTag = document.createElement('style');
        styleTag.textContent = css;
        iframe.contentDocument.head.append(styleTag);
      }

      //js last
      if (js) {
        //console.log("setJS", js);
        var scriptTag = document.createElement("script");
        scriptTag.type = "text/javascript";
        scriptTag.innerHTML = js;
        iframe.contentDocument.body.appendChild(scriptTag);
      }
    }

    //to detect when an element has been added to its parent
  }, {
    key: "onAppend",
    value: function onAppend(elem, f) {
      var observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (m) {
          if (m.addedNodes.length) {
            f(m.addedNodes);
          }
        });
      });
      //console.log("onAppend", elem);
      if (elem) {
        observer.observe(elem, {
          childList: true
        });
      }
    }
  }]);
  return MemoJSizer;
}();
exports["default"] = MemoJSizer;
//# sourceMappingURL=MemoJSizer.js.map