/**
 * Simple load helper
 * @param {*} url 
 * @param {*} callback 
 */
export function load(url, callback) {

    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && (this.status === 200 || this.status === 0)) {
            var response = this.responseText;
            callback(response);
        }
    };
    xhttp.open("GET", url);
    xhttp.send();
}

/**
 * Simple url param getter
 * @param {*} url 
 * @param {*} param 
 */
export function getUrlParameter(url, param) {

    var url = new URL(url);
    var result = url.searchParams.get(param);
    //console.log(param, "=", result);
    return result;
}