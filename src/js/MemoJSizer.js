//import markdown-it
import markdownIt from "markdown-it";
//import markdown-it.attrs
import markdownItAttrs from "markdown-it-attrs";
import markdownitHTML5Embed from 'markdown-it-html5-embed-fix';

//highlight.js configuration
import hljs from 'highlight.js/lib/core';
import javascript from 'highlight.js/lib/languages/javascript';
hljs.registerLanguage('javascript', javascript);
import html from 'highlight.js/lib/languages/xml';
hljs.registerLanguage('html', html);
import css from 'highlight.js/lib/languages/css';
hljs.registerLanguage('css', css);

import { load } from "./utils";
import { getUrlParameter } from "./utils";

export default class MemoJSizer {

    constructor(MDEntryPath) {

        this.MDEntryPath = MDEntryPath; //"md/index.md"

        //register the markdown converter
        this.converter = undefined;
        this.renderString = "render";
        this.renderStringRegEx = /render-[a-z]/;
        this.allRenderStringRegEx = /render-[a-z]/g;

        //list of all what we want to render
        this.toRender = {};

        this.setup();
    }

    //setup callback after page loading
    setup() {
        //adds the article tag with the needed id and class name
        document.body.innerHTML = "<article id='markdown-body' class='markdown-body'></article>";

        //launch the observer
        this.onAppend(document.getElementById("markdown-body"), (added) => {

            //walk through added nodes
            for (let i = 0; i < added.length; i++) {

                const elem = added[i];
                //console.log("added element = ", elem, elem.lang);

                //act only on <pre> element for detecting html, css and js code
                if (elem.tagName === "PRE") {

                    //grab the script from the element
                    let codeText = elem.innerText;
                    const codeTag = elem.children[0];
                    const scriptHTML = codeTag.innerHTML;
                    const renderMatch = scriptHTML.match(this.allRenderStringRegEx);

                    //console.log("match", renderMatch, scriptHTML);

                    if (renderMatch) {

                        for (let i = 0; i < renderMatch.length; i++) {

                            const renderRef = renderMatch[i];
                            //console.log("match str", renderRef);

                            //erase all the render-n instances
                            const tempScriptHTML = scriptHTML.replace(this.allRenderStringRegEx, "");
                            codeTag.innerHTML = tempScriptHTML;

                            codeText = codeTag.innerText;

                            //we must erase all links in the HTML file!
                            if (elem.lang === "html") {

                                //make a fake html document to manipulate the elements freely
                                const fakeDoc = document.createElement("html");
                                //affects the current html doc text (clean all highlight.js tags!)
                                fakeDoc.innerHTML = codeText;

                                //grab the elements we want to clear, and remove them
                                const links = fakeDoc.getElementsByTagName("link");

                                for (let i = 0; i < links.length; i++) {
                                    const link = links[i];
                                    link.parentElement.removeChild(link);
                                }

                                const scripts = fakeDoc.getElementsByTagName("script");

                                for (let i = 0; i < scripts.length; i++) {
                                    const script = scripts[i];
                                    script.parentElement.removeChild(script);
                                }
                                //replace the html source text with the new one
                                codeText = fakeDoc.innerHTML;
                                //console.log("html codeText", codeText);
                            }

                            //if don't have already the object to keep track of all codes
                            if (!Object.getOwnPropertyDescriptor(this.toRender, renderRef)) {

                                this.toRender[renderRef] = {}; //create {render-n : }
                                this.toRender[renderRef][elem.lang] = codeText;//fill {render-n : {lang: }}

                                this.toRender[renderRef]["elems"] = [];//fill {render-n : {lang: , elems: }}
                                this.toRender[renderRef]["elems"].push(elem);
                            }
                            else {

                                this.toRender[renderRef][elem.lang] = codeText;//fill {render-n : {lang: }}
                                this.toRender[renderRef]["elems"].push(elem);//add to {render-n : {lang: , elems: }}
                            }
                        }
                    }
                }

                //find the last element of the page, always the same "retour à l'index"
                if (elem.tagName === "P") {

                    if (elem.innerText === "Fin.") {

                        //console.log("the last item!");
                        //console.log("toRender", this.toRender);

                        //now generates the iFrames with the code we defined
                        this.addIFrames();
                    }
                }
            }

        });

        //check if we're already in one of the lesson page or not
        let MDPath = getUrlParameter(window.location.href, "mdpath");

        //if not, we'll get the main index for all lessons
        if (!MDPath) {
            MDPath = this.MDEntryPath;
        }
        //console.info("MDPath", MDPath);

        //markdown-it
        this.converter = new markdownIt("default", {
            "highlight": function (str, lang) {

                //console.log("lang", lang);

                if (lang && hljs.getLanguage(lang)) {
                    try {
                        var preTag = '<pre class="hljs" lang=' + lang + '><code class>' +
                            hljs.highlight(lang, str, true).value +
                            '</code></pre>';

                        return preTag;
                    }
                    catch (__) {
                    }
                }
            }
        });

        this.converter.use(markdownItAttrs);
        this.converter.use(markdownitHTML5Embed, {
            html5embed: {
                useImageSyntax: true, // Enables video/audio embed with ![]() syntax (default)
                useLinkSyntax: true,   // Enables video/audio embed with []() syntax
                attributes: {
                    video: 'class="video-player" controls'
                }
            },
            
        });

        //Load the markdown file and convert it to HTML with the converter
        load(MDPath, (text) => {

            //var html = converter.makeHtml(md);
            let html = this.converter.render(text);

            document.getElementById("markdown-body").innerHTML = html;

            //automatic generation of md files links for inner navigation
            let links = document.getElementsByTagName("a");

            for (let i = 0; i < links.length; i++) {
                let link = links[i];

                link.addEventListener("click", function (event) {

                    let url = new URL(this.href);

                    let newMDpath = getUrlParameter(url, "mdpath");

                    if (newMDpath) {
                        let newURL = window.location.href + "?mdpath=" + newMDpath;
                        //console.log("newURL", newURL);
                        window.location.replace(newURL);
                    }
                })
            }

        });
    }

    /**
     * adds rendering iFrames from code sources
     */
    addIFrames() {

        for (let renderObj in this.toRender) {

            let render = this.toRender[renderObj];

            //console.log("current render", render);

            //build the iFrame
            const iframe = document.createElement("iframe");
            iframe.src = 'about:blank';
            iframe.style.border = "solid gray 1px";
            iframe.style.width = "100%";
            iframe.style.height = "300px";
            iframe.style.overflow = "hidden";
            iframe.style.MozOverflow = "hidden";
            iframe.style.resize = "vertical";

            //what element is used to add an new one before
            let lastElem;

            let html, js, css;

            //iterate through all keys (html, css, ...)
            for (let key in render) {

                //finds the last HTML element to use as a ref
                if (key === "elems") {

                    lastElem = render[key][render[key].length - 1];
                    //console.log(lastElem);
                    //add it to the document to generate the contentWindow
                    lastElem.parentNode.insertBefore(iframe, lastElem.nextSibling);
                }

                if (key === "html") {
                    html = render[key];
                }

                if (key === "javascript") {
                    js = render[key];
                }

                if (key === "css") {
                    css = render[key];
                }
            }

            setTimeout(this.applyToIFrame, 100, iframe, html, css, js);

        }
    }

    applyToIFrame(iframe, html, css, js) {
        //change the content of the iFrame

        //html first, as we will change it's structure with JS afterward
        if (html) {
            //console.log("setHTML", html);
            iframe.contentDocument.write(html);
        }

        //css next
        if (css) {
            //console.log("setCSS", css);
            const styleTag = document.createElement('style');
            styleTag.textContent = css;
            iframe.contentDocument.head.append(styleTag);
        }

        //js last
        if (js) {
            //console.log("setJS", js);
            const scriptTag = document.createElement("script");
            scriptTag.type = "text/javascript";
            scriptTag.innerHTML = js;
            iframe.contentDocument.body.appendChild(scriptTag);
        }
    }

    //to detect when an element has been added to its parent
    onAppend(elem, f) {

        const observer = new MutationObserver(function (mutations) {

            mutations.forEach(function (m) {
                if (m.addedNodes.length) {
                    f(m.addedNodes)
                }
            });
        });
        //console.log("onAppend", elem);
        if (elem) {
            observer.observe(elem, { childList: true });
        }
    }
}