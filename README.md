# Mémo JavaScript - Librairie

MemoJS est un ensemble de scripts qui permettent d'interpréter des fichiers formatés en Markdown afin de réaliser des "cours en ligne" concernant la programmation en JavaScript dans les technologies du Web.

Il s'agit ici de la librarie, c'est à dire le moteur d'interprétation des fichiers .md uniquement. Cette librarie prend la forme d'un module npm qui peut être utilisé par quiconque souhaite réaliser des contenus à l'aide de MemoJS.

Il s'agit d'une forme écrite et consultable de certains cours proposés par Dominique Cunin dans le cadre de ses enseignements à [l'École Supérieure d'Art de Valence](http://esad-gv.fr).

Cette initiative fait suite à une requête des étudiants, qui désiraient pouvoir accéder à des ressources en dehors des cours et des entretiens individuels, sans qu'il s'agisse pour autant de cours en ligne tiers.

Un investissement plus important dans ce format a été fourni à l'occasion de la période de confinement imposée à la France et à de nombreux pays du monde pendant la pandémie du Coronavirus de 2019-2020.

## Structure de l'outil et fonctionnalités

Le langage [MarkDown](https://www.markdownguide.org/basic-syntax/) est utilisé pour créer les contenus à afficher. Il faut donc respecter la syntaxe de ce langage pour que les documents soient bien interprétés.

Quelques fonctionnalités "maison" ont été ajoutées spécifiquement pour cet outil. Il s'agit parfois d'extensions au MarkDown déjà disponibles à travers la librairie de conversion de MarkDown en HTML qui est ici utilisée ([markdown-it](https://github.com/markdown-it/markdown-it) et [markdown-it-attrs.browser](https://github.com/arve0/markdown-it-attrs)).

- Possibilité de définir une destination pour les liens :
```
[mon lien](url_du_lien){target=blank}
```

Les valeurs habituelles de l'attribut ```target``` des éléments ```<a>``` peuvent être utilisés.

- Possibilité de définir un style particulier pour un paragraphe :
```
Ma phrase.{style="opacity:0"}
```
Ici, le paragrpahe "Ma phrase." sera totalement transparent.

- Possibilité d'insérer une iFrame exécutant le code source présenté :

    Un parser complémentaire a été ajouté à ```highlight.js```, librairie qui permet de générer un affichage stylisé des codes sources en HTML en associant automatique une feuille de style de colorisation des mots clés, langage par langage.
    En ajoutant le préfix ```render-<lettre>``` au code source que vous voulez faire afficher dans une iFrame d'exécution, différentes sources peuvent être associées entre elles.

```
//TODO
```

## Usage

### Pour les Développeurs

Vous pouvez transformer les sources sur une copie local à votre machine, ou dans une branche que vous gérez.

Babel est utilisé pour transpiler les sources ES6 en fichiers distribuables largement, situés dans ```/dist``` :

> npm run build-dist

Pour une mise à jour au fil du travail :

> npm run build-dist-watch

Le package npm est deployé régulièrement à l'aide de ```np``` afin d'assurer une bonne gestion des numéros de versions.

Webpack permet de créer un bundle compilé distribuale situé dans ```/build```

> npm run build-bundle

### Pour les éditeurs de contenus

Pour utiliser MemoJS pour la création de vos propre contenus, il est préférable d'utiliser les projets préparés à cet effet dans le groupe dédié : https://gitlab.com/esad-gv1

